clear
set more off
capture log close
clear matrix
clear mata
set mat  900
set maxvar 9000
set scheme lean2
global mydate: di %tdCYND date(c(current_date), "DMY")

*net search runmplus

global path_in	   		"C:\Wetzel\DEAS\01_Daten\03_Panel"
global path_temp 		"C:\Wetzel\Diss\01_DA\07_Ruhestand_DEAS2\temp"
global path_log  		"C:\Wetzel\Diss\01_DA\07_Ruhestand_DEAS2\log"
global path_out			"C:\Wetzel\Diss\01_DA\07_Ruhestand_DEAS2\dta"
global path_do			"C:\Wetzel\Diss\01_DA\07_Ruhestand_DEAS2\do"
global path_tex			"C:\Wetzel\Diss\01_DA\07_Ruhestand_DEAS2\tex"
global path_m			"C:\Wetzel\MA_Wetzel\Ruhestand_DEAS\Data"
global path_g			"C:\Wetzel\MA_Wetzel\Ruhestand_DEAS\graph"

cap mkdir $path_temp
cap mkdir $path_log
cap mkdir $path_out
cap mkdir $path_tex
cap mkdir $path_m


log using "$path_log\ruhestand_ds2$mydate.smcl", replace
di "$path_do -   $mydate    "

set scheme lean1
set more off

global gropts ///
	plot1opts(lcolor(gs9) mcolor(gs9)) ci1opts(lcolor(gs9))						///
	plot2opts(lcolor(cranberry) mcolor(cranberry)) ci2opts(lcolor(cranberry))	///
	///	plot3opts(lcolor(black) mcolor(black)) ci3opts(lcolor(black))	///
	 	 ///		//nodraw ylabel(30(5)60)
	xline(7, lpattern(shortdash)) legend(row(1) pos(6))
	
global anyw for any _1 _2 _3 _4 _5:	
	
**********************************************************************************************************************
** DATUM LETZTE BEARBEITUNG DO-FILE: 16-07-14                                              	        **
** ERSTER  BEARBEITER:               MWe                                                    			            ** 
** LETZTER BEARBEITER:               MWe                                       				                        **
**********************************************************************************************************************

**********************************************************************************************************************
** DO-FILE: 								                        	    	   				                    **
** BESCHREIBUNG (maximal ausführlich: Was soll wie gemacht werden...			   									**		
**********************************************************************************************************************
/* 

Do-File for article:
"Level and Change in Economic, Social, and Personal Resources for People Retiring 
from Paid Work and Other Labour Market Statuses" 
with Bowen & Huxhold, in European Journal of Ageing
DOI: 10.1007/s10433-019-00516-y

*/
**********************************************************************************************************************
** 0: VORARBEITEN	 				                                                              				**
**********************************************************************************************************************
* 2 Eingangsvariablenlisten: keepvars aus Excel-Sheet: Modellvariablen 
*							 keepvars2 sind allgemeine, immer benötigte Variablen
global keepvars3 fallnum welle intjahr stich alter nat_ewo bildung3 isced int_do
		 
			
do "$path_do/01_keepvars_l.do"		// global mit keepvars


use $keepvars1  $keepvars2 $keepvars3 using "$path_in\DEAS_P96-14_l_v0.2b.dta", clear

for any 2 3: replace aee_oecd = aee_oecdX if welle == X

* Umbenennungen
tabstat isced pc1, s(n)
rename isced bil3
rename pc1 sex
replace intjahr = 2014 if welle == 5


saveold "$path_temp\ruhestand_ds_1_l.dta", replace
d, s

****************************************************************************************
***! 1: Generierung der Variablen
****************************************************************************************

run "$path_do/02_gen_vars_l.do"		//

* Erwerbstätigkeit im Ruhestand	
/*
sort falln welle
gen reerw     = inlist(erw,1,3) & erw[_n+1] == 2 & fallnum == fallnum[_n+1] & ///
						welle == welle[_n+1] - 1 & Xpc102[_n+1] == 1
tab reerw welle	, col							
replace reerw = 2 if inlist(erw,2) & erw[_n+1] == 2 & fallnum == fallnum[_n+1] & ///
						welle == welle[_n+1] - 1 & Xpc102 == 2 & Xpc102[_n+1] == 1
count if fall == fall[_n+1] & Xpc102 == 2 & Xpc102[_n+1] == 1	*/


drop if welle == 4
recode welle (5=4), copyrest

saveold "$path_temp\ruhestand_ds_2_l.dta", replace

****************************************************************************************
***! Pfadanalyse
****************************************************************************************
* do "$path_do/03_gen_ruhestzp_3.do"		// 
use "$path_temp\ruhestand_ds_2_l.dta", clear

recode pc106 (1 = 1)(2 = 3)(8 = 4)(4 = 5)(5 6 7 9 = 6), gen(path) 
	lab def job 1 "ft. empl." 2 "pt. empl." 3 "unempl." 4 "homemaker" 5 "early ret." 6 "else"
lab val path job

gen dist = .a
for any 1 2 3 4 \ any 1996 2002 2008 2014: replace dist = Y - rsj_kons if welle == X 
recode dist (0/6=1)(else=0)
tab path 					 if dist == 1
for any 1 2 3: tab path [aweight=weight_id]  if dist == 1 & welle == X
tab path [aweight=weight_id]  if dist == 1 & welle == 3





// Transitionen zwischen 2 Beobachtungen
xtset fallnum welle
* svyset [pweight=weight_id]		// Gewichtung im long-format nicht möglich
* svy: tab sex
* tab sex [aweight=weight_id]

xttrans erw if inlist(welle,1,2)
tokenize 1 2 3
foreach k of numlist 2 3 4 {
	di "`1' zu `k'"
	xttrans erw if inlist(welle,`1',`k')
	macro shift
	}

gen ret = inlist(erw,1,3) & erw[_n+1] == 2 & fallnum == fallnum[_n+1] & ///
						welle == welle[_n+1] - 1
tab ret welle	, col	
tab ret welle	[aweight=weight_id], col	
*       ret |         1          2          3 |     Total
*           |     94.07      95.05      94.56 |     94.50 		
*           |      5.93       4.95       5.44 |      5.50 		N = 1104

gen reerw     = inlist(erw,1,3) & erw[_n+1] == 2 & fallnum == fallnum[_n+1] & ///
						welle == welle[_n+1] - 1 & Xpc102[_n+1] == 1
tab reerw welle	, col							
					

saveold "$path_temp\ruhestand_ds_3_l.dta", replace

****************************************************************************************
***! Selektion
****************************************************************************************
use "$path_temp\ruhestand_ds_3_l.dta", clear

gen nw_fam1 = nwgroesse - nwgr_afam
tab anzphy
replace anzphy = -anzphy + 11
tab anzphy
gen linc = log(aee_oecd)

*
* Selektivitätsanalysen
*
*
// Nur Basisbefragte
tab welle stich
count if stich == welle & welle != 4
tab welle stich if inrange(alter,55,65)
count if stich == welle & inrange(alter,55,65) & welle != 4


// Auswahl der Wiederholungsbefragten
gen flag = fallnum == fallnum[_n+1] & ///
		   (welle == welle[_n+1] - 1 & (stich == welle)) | 	///
		   (welle == welle[_n-1] + 1 & (stich == welle[_n-1]))
tab flag 
tab flag if stich == welle & inrange(alter,55,65) & welle != 4
count if flag == 1 & stich == welle & inrange(alter,55,65) & welle != 4

// Nur Basisbefragte im Altersrange
bys fallnum: egen flag1 = total((inrange(alter,55,65)  & (stich == welle)  & welle != 4))
tab flag1 if falln != fall[_n+1]
keep if flag1 == 1
		   
// nur Wiederholungsbefragt mit T0 im Altersrange 55 bis 65
gen flag2 = fallnum == fallnum[_n+1] & ///
		   (welle == welle[_n+1] - 1 & inrange(alter[_n],55,65)  & (stich == welle)) | 	///
		   (welle == welle[_n-1] + 1 & inrange(alter[_n-1],55,65) & (stich == welle[_n-1]))		   
sort falln
tab flag2 if falln != fall[_n+1]
		   
global logvars i.welle c.alter i.bil3 sex westost ///
			   anzphy aktanzg13 support nwgr_afam nw_fam1 linc
			   
global select  inrange(alter,55,65) & stich == welle & welle != 4
tab flag if $select
tab alter if $select 
			
tabstat  aee_oecd anzphy aktanzg13 support nwgr_afam nw_fam1 support ///
			if $select , s(mean sd n) by(flag)
			
tabstat  welle alter bil3 sex westost migrat ///
			if $select , s(mean sd n) by(flag)

logistic flag $logvars i.erw if $select


coefplot, drop(_cons) xline(1) eform xtitle(Odds ratio)	name(loggr1, replace) ///
		ciopts(recast(rcap)) swap 		

* gsem (	flag <- $logvars	) if $select, logit nocapslatent
* estat eform

preserve
recode bil3 (. 1 2 = 0)(3=1) // 1 missing
for any welle bil3: tab X, gen(X)
* replace welle = welle - 1
global logvars2 alter bil31 sex westost ///
			   anzphy aktanzg13 support nwgr_afam nw_fam1 linc
			   
mdesc flag $logvars2 if $select
foreach v in `r(miss_vars)' {
	di "~~~`v'~~~~"
	foreach n1 of numlist 0 1 {
	foreach n2 of numlist 1 2 {
	foreach n3 of numlist 1 2 3 {
		qui sum `v' if bil3 == `n1' & sex == `n2' & welle == `n3'
		replace `v' = `r(mean)' if mi(`v') & bil3 == `n1' & sex == `n2' ///
										& welle == `n3'
		}	
		}
		}
	}
* tab1 	$logvars2 if $select, m
	
logistic flag $logvars i.erw if $select
coefplot, drop(_cons) xline(1) eform xtitle(Odds ratio)	name(loggr2, replace) ///
		ciopts(recast(rcap)) swap 	
graph combine loggr1 loggr2, ycom xcom		
restore	
		
*
* Auswahl der RuheständlerInnen
*
keep if flag2 == 1

* nur wenn Ret aus Basisbefragung
tab ret if $select
tab ret if welle == stich
tab ret if welle != stich
replace ret = 0 if ret == 1 &  welle != stich
tab ret welle	, col
// 666 Beobachtungen aus Basis, 78 nicht


// nur Wiederholungsbefragte
* Keep nur Basis und Follow-Up
*gen flag = fallnum == fallnum[_n+1] & ///
*		   (welle == welle[_n+1] - 1 & (stich == welle)) | 	///
*		   (welle == welle[_n-1] + 1 & (stich == welle[_n-1]))
		   
bys fallnum: egen tflag = total(flag)
tab tflag
* Follow-Up-Rates
tab tflag welle if stich == welle, col	  
*    tflag |         1          2          3          4 |     Total
*     0    |     68.50      67.57      58.60     100.00 |     74.70 
*     2    |     31.50      32.43      41.40       0.00 |     25.30 
*    Total |     4,838      3,084      6,205      6,002 |    20,129 
 
keep if flag == 1
drop flag tflag


tab ret		// 776 RuheständlerInnen insgesamt aus Basisbefragung


tab welle stich 

// nur RuheständlerInnen
bys fallnum (welle): egen tret = total(ret)
tab ret tret
keep if tret == 1

*
* Distanz zum Ruhestand
*
// Ruhestandsjahr
tab pc104_2 if fall == fall[_n-1] & tret == 1 , m	// 41 ohne Ruhestandsjahr
tab2 rsj pc104_2 if fall == fall[_n-1] & tret == 1 , m	// 28 Ruhestandsjahre aus anderer Welle

bys fallnum (welle): egen rj = total(pc104_2)
replace rj = rsj if rj == 0 
tab rj if tret == 1 , m

// ersetzen der letzten 12 Missings durch Mittelwertsimputation
gen period = welle 
replace period = welle[_n-1] if fall == fall[_n-1]
tab period, m
for any 1 2 3 \ any 1999 2005 2011: replace rj = Y if mi(rj) & period == X

tab rj period
count if fall == fall[_n+1] & rj != rj[_n+1]

// widersprüchliche Aussagen zu Ruhestandsjahr (nicht zwischen 2 Beobachtungszeitpunkten)
gen flag = .z
for any 1 2 3 \ any 2002 2008 2014: replace flag = 1 if (Y - rj) > 6 & period == X
for any 1 2 3 \ any 2002 2008 2014: replace flag = 1 if (Y - rj) < 0 & period == X
tab flag	// 66 Ausreiser
list fallnum welle erw alter period rj a_rs if flag == 1, nol
// werden auch durch Mittelwerte ersetzt
for any 1 2 3 \ any 2002 2008 2014: replace rj = Y if period == X & flag == 1
tab rj period


*
* Alter beim Ruhestand
*
list fallnum if pc2_3 != pc2_3[_n+1] & fal == fall[_n+1]

drop    a_rs
gen 	a_rs = rj - pc2_3		// geburtsjahr

count if a_rs  !=  a_rs[_n+1] & fal == fall[_n+1]	// über wellen konstant: gut!
tab2 flag a_rs if fallnum == fallnum[_n+1], m

drop flag2
gen flag2 = fallnum == fallnum[_n+1] & ///
		   (welle == welle[_n+1] - 1 & inrange(a_rs[_n],60,65)) | 	///
		   (welle == welle[_n-1] + 1 & inrange(a_rs[_n-1],60,65))
tab ret flag2 
tab a_rs flag if flag2 == 1, m
tab a_rs if flag  == 1 & flag2 != 1

drop flag

tab flag2 if fall != fall[_n+1]

keep if flag2 == 1
drop flag*

*
* Distanz zum Ruhestand
*
drop    d_rs
gen 	d_rs = .z
for any 1 2 3 \ any 2002 2008 2014: replace	d_rs = Y - rj  if period == X
tab d_rs
count if d_rs  !=  d_rs[_n+1] & fal == fall[_n+1]	// über wellen konstant: gut!

*
* Kohorte
*
tab pc2_3
gen cohort = inrange(pc2_3,1931,1943)

saveold "$path_temp\ruhestand_ds_4.dta", replace

use "$path_temp\ruhestand_ds_4.dta", clear

*
* Deskriptives
* 	
replace westost = westost[_n-1] if fallnum==fallnum[_n-1] & mi(westost)
replace bil3 = bil3[_n-1] if fallnum==fallnum[_n-1] & mi(bil3)

bys fallnum (welle): gen n=_n
gen gr = erw if n==1
replace gr = gr[_n-1] if fallnum==fallnum[_n-1] & mi(gr)


recode gr (3=2), copyrest
tab gr	

tabstat linc anzphy aktanzg13 support nwgr_afam nw_fam1 support sup ///
			d_rs bil3 welle sex a_rs westost, s(mean sd n) by(gr)

// Description of the "non-working" //
preserve
tab pc101 gr if n == 1, m col
recode pc101 (1 3 = 2)(2=1)(6 7 9 .c = 4)(8=3), gen(nowo)
tab nowo if n == 1 & gr == 2, m

			
			
*
* Vorbereitungen für CHANGE SCORE ANALYSE
* 
*			
do "$path_do/05_stumpf.do"		// global mit keepvars

*	WIDE-format
rename * *_
rename fallnum_ fallnum
keep fallnum welle nowo $stumpf
bys fallnum: gen n = _n			

reshape wide $stumpf nowo welle_, i(fallnum) j(n)

tab a_rs_1 , m
tab pc2_3_1 welle_1, m
tab pc2_3_1 welle_2, m

renpfix aee_oecd oecd 
renpfix nwgroesse nwgr
renpfix nwgr_afam nwafam
rename zzrscore* zzr*
rename pc802neu* pc802*	

recode bil3_1 (1 2=0)(3=1)

sum d_rs_1
gen md_rs = d_rs_1 - `r(mean)'
sum md_rs

sum welle_1
replace welle_1 = welle_1 - `r(mean)'
recode erw_1 (1=0)(3=1)
sum westost_1
replace westost_1 = westost_1 - `r(mean)'
sum sex_1
replace sex_1 = sex_1 - `r(mean)'
sum cohort_1
replace cohort_1 = cohort_1 - `r(mean)'
sum bil3_1
replace bil3_1 = bil3_1 - `r(mean)'
sum a_rs_1
replace a_rs_1 = a_rs_1 - `r(mean)'
sum welle_1 westost_1 sex_1 cohort_1 bil3_1 a_rs_1

tabstat md_rs bil3_1 welle_1 sex_1 a_rs_1 westost_1, s(mean sd n) by(gr_1)

for any A: mat X = J(10,30,.)	

// pc802 oecd eink
// aktanzs2 aktint aktanzg  
// nwgr nwafam ehramt
// pc501 anzphy sf36
// hope zzr lone6
// lz pa na	

gen nwfam_1 = nwgr_1 - nwafam_1
gen nwfam_2 = nwgr_2 - nwafam_2

*
* Item-Non-Response: Missings
*
mdesc welle_1 westost_1 sex_1 bil3_1 a_rs_1 md_rs ///
	oecd* anzphy* nwafam* nwfam* aktanzg13* support*

*******
// non-working analyses
for any oecd anzphy nwafam nwfam aktanzg13 support: ///
	ttest X_1 == X_2 if nowo_1 == 4
	
	
saveold "$path_temp\ruhestand_ds_5.dta", replace

use "$path_temp\ruhestand_ds_5.dta", clear
cap log close
log using "$path_log\ruhestand_ds2_models_$mydate.smcl", append
eststo clear
preserve
set more off
local i = 1
foreach v in oecd anzphy nwafam nwfam aktanzg13 support {	/// sup 
		/// aktanzg lz na pa nwafam ehramt nwgr pc501 anzphy sf36 aktint lone6 hope pc802 oecd eink aktanzs2 { 
		///	 pc501 anzphy sf36 hope zzr lone6 lz pa na	{
		/// pc802 lz na pa 

	*preserve
	*local v oecd
	*local i = 1
	di "~~~~~~ variable `v' "
	
	sum		`v'_1	
	*	matrix A[1,`i'] = `r(mean)'			// mean
	*	matrix A[2,`i'] = `r(sd)'			// se
	*	matrix A[3,`i'] = `r(N)'			// N
		* mat li A

	gen z_`v'_1 = ( `v'_1 - `r(mean)' ) / `r(sd)'	
	gen z_`v'_2 = ( `v'_2 - `r(mean)' ) / `r(sd)'	
	
	gen d`v'  = (`v'_2 - `v'_1) 
	sum d`v'
	gen zd`v' = ( d`v' - `r(mean)' ) / `r(sd)'
	sum zd`v' 

			
	** ALL PARAMETERS FREE			
	runmplus   z_`v'_1 z_`v'_2 erw_1 welle_1 westost_1 sex_1 a_rs_1 bil3_1 md_rs , 			///
		model( z_`v'_2 on z_`v'_1@1; 		///
			   d_`v' by z_`v'_2@1;	///
			   d_`v' on z_`v'_1;	///
			   z_`v'_2@0;  	///
							///
			   [z_`v'_1](id); [d_`v'](cd); 	///
			   [z_`v'_2@0 welle_1@0 westost_1@0 sex_1@0 bil3_1@0 a_rs_1@0 md_rs@0 ]; 	///
			   ///
			   welle_1 	with westost_1 sex_1 bil3_1 a_rs_1 md_rs ;	///
			   westost_1 with sex_1 bil3_1 a_rs_1 md_rs ;	/// 
			   sex_1 	with bil3_1 a_rs_1 md_rs ;	/// 
			   bil3_1 with a_rs_1 md_rs ;	///
			   a_rs_1 with md_rs ;	///
			   ///
			   z_`v'_1 d_`v' on  welle_1 westost_1 sex_1 a_rs_1 bil3_1 md_rs ;	///
			   ///
			   model dir:	///
			   z_`v'_2 on z_`v'_1@1; 	///
			   d_`v' by z_`v'_2@1;	///
			   d_`v' on z_`v'_1;	///
			   z_`v'_2@0;  	///
			   	///
			   [z_`v'_1](id); [d_`v'](cd); 	///
			   [z_`v'_2@0 welle_1@0 westost_1@0 sex_1@0 bil3_1@0 a_rs_1@0 md_rs@0 ]; 	///
			   	///
			   welle_1 	with westost_1 sex_1 bil3_1 a_rs_1 md_rs ;	///
			   westost_1 with sex_1 bil3_1 a_rs_1 md_rs ;	/// 
			   sex_1 	with bil3_1 a_rs_1 md_rs ;	/// 
			   bil3_1 with a_rs_1 md_rs ;	///
			   a_rs_1 with md_rs ;	///
			   ///
			   z_`v'_1 d_`v' on  welle_1 westost_1 sex_1 a_rs_1 bil3_1 md_rs ;	///
			   	///
			   model ind:	///
			   z_`v'_2 on z_`v'_1@1; 	///
			   d_`v' by z_`v'_2@1;	///
			   d_`v' on z_`v'_1;	///
			   z_`v'_2@0;  	///
			   	///
			   [z_`v'_1](id2); [d_`v'](cd2); 	///
			   [z_`v'_2@0 welle_1@0 westost_1@0 sex_1@0 bil3_1@0 a_rs_1@0 md_rs@0 ]; 	///
			   	///
			   welle_1 	with westost_1 sex_1 bil3_1 a_rs_1 md_rs ;	///
			   westost_1 with sex_1 bil3_1 a_rs_1 md_rs ;	/// 
			   sex_1 	with bil3_1 a_rs_1 md_rs ;	/// 
			   bil3_1 with a_rs_1 md_rs ;	///
			   a_rs_1 with md_rs ;	///
			   ///
			   z_`v'_1 d_`v' on  welle_1 westost_1 sex_1 a_rs_1 bil3_1 md_rs ;	///
			   	///
			   )	///
			    SAVELOGfile($path_log/`v'_1a, replace) title("ALL PARAMETERS FREE!!!")	///
			   iterations(10000) type(general) variable(GROUPING = erw_1(0=dir 1=ind))	///
			   // log(off)

	di "*****************************" _n "***** `v' ~ ALL PARAMETERS FREE *********"
	di "status: `r(termination)' - RMSEA: `r(RMSEA)'	- Chi2:  `r(chisquare)' 	- DF: `r(chisquare_df)'"
	/*mat li r(estimate)
	mat li r(z)	   
	di "von 	z = 1,645 "	*/
			   	
	** FIXED INTERCEPTS - FREE SLOPES			
	runmplus   z_`v'_1 z_`v'_2 erw_1 welle_1 westost_1 sex_1 a_rs_1 bil3_1 md_rs , 			///
		model( z_`v'_2 on z_`v'_1@1; 		///
			   d_`v' by z_`v'_2@1;	///
			   d_`v' on z_`v'_1;	///
			   z_`v'_2@0;  	///
							///
			   [z_`v'_1](id); [d_`v'](cd); 	///
			   [z_`v'_2@0 welle_1@0 westost_1@0 sex_1@0 bil3_1@0 a_rs_1@0 md_rs@0 ]; 	///
			   ///
			   welle_1 	with westost_1 sex_1 bil3_1 a_rs_1 md_rs ;	///
			   westost_1 with sex_1 bil3_1 a_rs_1 md_rs ;	/// 
			   sex_1 	with bil3_1 a_rs_1 md_rs ;	/// 
			   bil3_1 with a_rs_1 md_rs ;	///
			   a_rs_1 with md_rs ;	///
			   ///
			   z_`v'_1 d_`v' on  welle_1 westost_1 sex_1 a_rs_1 bil3_1 md_rs ;	///
			   ///
			   model dir:	///
			   z_`v'_2 on z_`v'_1@1; 	///
			   d_`v' by z_`v'_2@1;	///
			   d_`v' on z_`v'_1;	///
			   z_`v'_2@0;  	///
			   	///
			   [z_`v'_1](id); [d_`v'](cd); 	///
			   [z_`v'_2@0 welle_1@0 westost_1@0 sex_1@0 bil3_1@0 a_rs_1@0 md_rs@0 ]; 	///
			   	///
			   welle_1 	with westost_1 sex_1 bil3_1 a_rs_1 md_rs ;	///
			   westost_1 with sex_1 bil3_1 a_rs_1 md_rs ;	/// 
			   sex_1 	with bil3_1 a_rs_1 md_rs ;	/// 
			   bil3_1 with a_rs_1 md_rs ;	///
			   a_rs_1 with md_rs ;	///
			   ///
			   z_`v'_1 d_`v' on  welle_1 westost_1 sex_1 a_rs_1 bil3_1 md_rs ;	///
			   	///
			   model ind:	///
			   z_`v'_2 on z_`v'_1@1; 	///
			   d_`v' by z_`v'_2@1;	///
			   d_`v' on z_`v'_1;	///
			   z_`v'_2@0;  	///
			   	///
			   [z_`v'_1](id); [d_`v'](cd2); 	///
			   [z_`v'_2@0 welle_1@0 westost_1@0 sex_1@0 bil3_1@0 a_rs_1@0 md_rs@0 ]; 	///
			   	///
			   welle_1 	with westost_1 sex_1 bil3_1 a_rs_1 md_rs ;	///
			   westost_1 with sex_1 bil3_1 a_rs_1 md_rs ;	/// 
			   sex_1 	with bil3_1 a_rs_1 md_rs ;	/// 
			   bil3_1 with a_rs_1 md_rs ;	///
			   a_rs_1 with md_rs ;	///
			   ///
			   z_`v'_1 d_`v' on  welle_1 westost_1 sex_1 a_rs_1 bil3_1 md_rs ;	///
			   	///
			   )	///
			   title("FIXED INTERCEPTS - FREE SLOPES")	///
			   iterations(10000) type(general) variable(GROUPING = erw_1(0=dir 1=ind))	///
			   SAVELOGfile($path_log/`v'_1b, add) //log(off)

	di "*****************************" _n "***** `v' ~ FIXED INTERCEPTS AND FREE SLOPES *********"
	di "status: `r(termination)' - RMSEA: `r(RMSEA)'	- Chi2:  `r(chisquare)' 	- DF: `r(chisquare_df)'"
				
	** FIXED INTERCEPTS AND FIXED SLOPES			
	runmplus   z_`v'_1 z_`v'_2 erw_1 welle_1 westost_1 sex_1 a_rs_1 bil3_1 md_rs , 			///
		model( z_`v'_2 on z_`v'_1@1; 		///
			   d_`v' by z_`v'_2@1;	///
			   d_`v' on z_`v'_1;	///
			   z_`v'_2@0;  	///
							///
			   [z_`v'_1](id); [d_`v'](cd); 	///
			   [z_`v'_2@0 welle_1@0 westost_1@0 sex_1@0 bil3_1@0 a_rs_1@0 md_rs@0 ]; 	///
			   ///
			   welle_1 	with westost_1 sex_1 bil3_1 a_rs_1 md_rs ;	///
			   westost_1 with sex_1 bil3_1 a_rs_1 md_rs ;	/// 
			   sex_1 	with bil3_1 a_rs_1 md_rs ;	/// 
			   bil3_1 with a_rs_1 md_rs ;	///
			   a_rs_1 with md_rs ;	///
			   ///
			   z_`v'_1 d_`v' on  welle_1 westost_1 sex_1 a_rs_1 bil3_1 md_rs ;	///
			   ///
			   model dir:	///
			   z_`v'_2 on z_`v'_1@1; 	///
			   d_`v' by z_`v'_2@1;	///
			   d_`v' on z_`v'_1;	///
			   z_`v'_2@0;  	///
			   	///
			   [z_`v'_1](id); [d_`v'](cd); 	///
			   [z_`v'_2@0 welle_1@0 westost_1@0 sex_1@0 bil3_1@0 a_rs_1@0 md_rs@0 ]; 	///
			   	///
			   welle_1 	with westost_1 sex_1 bil3_1 a_rs_1 md_rs ;	///
			   westost_1 with sex_1 bil3_1 a_rs_1 md_rs ;	/// 
			   sex_1 	with bil3_1 a_rs_1 md_rs ;	/// 
			   bil3_1 with a_rs_1 md_rs ;	///
			   a_rs_1 with md_rs ;	///
			   ///
			   z_`v'_1 d_`v' on  welle_1 westost_1 sex_1 a_rs_1 bil3_1 md_rs ;	///
			   	///
			   model ind:	///
			   z_`v'_2 on z_`v'_1@1; 	///
			   d_`v' by z_`v'_2@1;	///
			   d_`v' on z_`v'_1;	///
			   z_`v'_2@0;  	///
			   	///
			   [z_`v'_1](id); [d_`v'](cd); 	///
			   [z_`v'_2@0 welle_1@0 westost_1@0 sex_1@0 bil3_1@0 a_rs_1@0 md_rs@0 ]; 	///
			   	///
			   welle_1 	with westost_1 sex_1 bil3_1 a_rs_1 md_rs ;	///
			   westost_1 with sex_1 bil3_1 a_rs_1 md_rs ;	/// 
			   sex_1 	with bil3_1 a_rs_1 md_rs ;	/// 
			   bil3_1 with a_rs_1 md_rs ;	///
			   a_rs_1 with md_rs ;	///
			   ///
			   z_`v'_1 d_`v' on  welle_1 westost_1 sex_1 a_rs_1 bil3_1 md_rs ;	///
			   	///
			   )	///
			   title("FIXED INTERCEPTS AND FIXED SLOPES")	///
			   iterations(10000) type(general) variable(GROUPING = erw_1(0=dir 1=ind))	///
			   SAVELOGfile($path_log/`v'_1c, add) //log(off)
			   
	di "*****************************" _n "***** `v' ~ FIXED INTERCEPTS AND FIXED SLOPES *********"
	di "status: `r(termination)' - RMSEA: `r(RMSEA)'	- Chi2:  `r(chisquare)' 	- DF: `r(chisquare_df)'"


	** FREE INTERCEPTS AND FIXED SLOPES			
	runmplus   z_`v'_1 z_`v'_2 erw_1 welle_1 westost_1 sex_1 a_rs_1 bil3_1 md_rs , 			///
		model( z_`v'_2 on z_`v'_1@1; 		///
			   d_`v' by z_`v'_2@1;	///
			   d_`v' on z_`v'_1;	///
			   z_`v'_2@0;  	///
							///
			   [z_`v'_1](id); [d_`v'](cd); 	///
			   [z_`v'_2@0 welle_1@0 westost_1@0 sex_1@0 bil3_1@0 a_rs_1@0 md_rs@0 ]; 	///
			   ///
			   welle_1 	with westost_1 sex_1 bil3_1 a_rs_1 md_rs ;	///
			   westost_1 with sex_1 bil3_1 a_rs_1 md_rs ;	/// 
			   sex_1 	with bil3_1 a_rs_1 md_rs ;	/// 
			   bil3_1 with a_rs_1 md_rs ;	///
			   a_rs_1 with md_rs ;	///
			   ///
			   z_`v'_1 d_`v' on  welle_1 westost_1 sex_1 a_rs_1 bil3_1 md_rs ;	///
			   ///
			   model dir:	///
			   z_`v'_2 on z_`v'_1@1; 	///
			   d_`v' by z_`v'_2@1;	///
			   d_`v' on z_`v'_1;	///
			   z_`v'_2@0;  	///
			   	///
			   [z_`v'_1](id); [d_`v'](cd); 	///
			   [z_`v'_2@0 welle_1@0 westost_1@0 sex_1@0 bil3_1@0 a_rs_1@0 md_rs@0 ]; 	///
			   	///
			   welle_1 	with westost_1 sex_1 bil3_1 a_rs_1 md_rs ;	///
			   westost_1 with sex_1 bil3_1 a_rs_1 md_rs ;	/// 
			   sex_1 	with bil3_1 a_rs_1 md_rs ;	/// 
			   bil3_1 with a_rs_1 md_rs ;	///
			   a_rs_1 with md_rs ;	///
			   ///
			   z_`v'_1 d_`v' on  welle_1 westost_1 sex_1 a_rs_1 bil3_1 md_rs ;	///
			   	///
			   model ind:	///
			   z_`v'_2 on z_`v'_1@1; 	///
			   d_`v' by z_`v'_2@1;	///
			   d_`v' on z_`v'_1;	///
			   z_`v'_2@0;  	///
			   	///
			   [z_`v'_1](id2); [d_`v'](cd); 	///
			   [z_`v'_2@0 welle_1@0 westost_1@0 sex_1@0 bil3_1@0 a_rs_1@0 md_rs@0 ]; 	///
			   	///
			   welle_1 	with westost_1 sex_1 bil3_1 a_rs_1 md_rs ;	///
			   westost_1 with sex_1 bil3_1 a_rs_1 md_rs ;	/// 
			   sex_1 	with bil3_1 a_rs_1 md_rs ;	/// 
			   bil3_1 with a_rs_1 md_rs ;	///
			   a_rs_1 with md_rs ;	///
			   ///
			   z_`v'_1 d_`v' on  welle_1 westost_1 sex_1 a_rs_1 bil3_1 md_rs ;	///
			   	///
			   )	///
			   title("FREE INTERCEPTS AND FIXED SLOPES")	///
			   iterations(10000) type(general) variable(GROUPING = erw_1(0=dir 1=ind))	///
			   SAVELOGfile($path_log/`v'_1d, add) //log(off)
			   
	di "*****************************" _n "***** `v' ~ FREE INTERCEPTS AND FIXED SLOPES *********"			   
	di "status: `r(termination)' - RMSEA: `r(RMSEA)'	- Chi2:  `r(chisquare)' 	- DF: `r(chisquare_df)'"
	
	local i = `i' + 1
	local varlist `varlist' `v'_`k'`1'	
	}
restore
	
**
** Null-Setzen von Parametern
**
**
	
** HH-Einkommen: all free

	preserve
	local v oecd
	di "~~~~~~ variable `v' "
	
	sum		`v'_1	
	gen z_`v'_1 = ( `v'_1 - `r(mean)' ) / `r(sd)'	
	gen z_`v'_2 = ( `v'_2 - `r(mean)' ) / `r(sd)'	
	
	gen d`v'  = (`v'_2 - `v'_1) 
	sum d`v'
	gen zd`v' = ( d`v' - `r(mean)' ) / `r(sd)'
	sum zd`v' 
	
	runmplus   z_`v'_1 z_`v'_2 erw_1 welle_1 westost_1 sex_1 a_rs_1 bil3_1 md_rs , 			///
		model( z_`v'_2 on z_`v'_1@1; 		///
			   d_`v' by z_`v'_2@1;	///
			   d_`v' on z_`v'_1;	///
			   z_`v'_2@0;  	///
							///
			   [z_`v'_1](id); [d_`v'](cd); 	///
			   [z_`v'_2@0 welle_1@0 westost_1@0 sex_1@0 bil3_1@0 a_rs_1@0 md_rs@0 ]; 	///
			   ///
			   welle_1 	with westost_1 sex_1 bil3_1 a_rs_1 md_rs ;	///
			   westost_1 with sex_1 bil3_1 a_rs_1 md_rs ;	/// 
			   sex_1 	with bil3_1 a_rs_1 md_rs ;	/// 
			   bil3_1 with a_rs_1 md_rs ;	///
			   a_rs_1 with md_rs ;	///
			   ///
			   z_`v'_1 d_`v' on  welle_1 westost_1 sex_1 a_rs_1 bil3_1 md_rs ;	///
			   ///
			   model dir:	///
			   z_`v'_2 on z_`v'_1@1; 	///
			   d_`v' by z_`v'_2@1;	///
			   d_`v' on z_`v'_1;	///
			   z_`v'_2@0;  	///
			   	///
			   [z_`v'_1](id); [d_`v'](cd); 	///
			   [z_`v'_2@0 welle_1@0 westost_1@0 sex_1@0 bil3_1@0 a_rs_1@0 md_rs@0 ]; 	///
			   	///
			   welle_1 	with westost_1 sex_1 bil3_1 a_rs_1 md_rs ;	///
			   westost_1 with sex_1 bil3_1 a_rs_1 md_rs ;	/// 
			   sex_1 	with bil3_1 a_rs_1 md_rs ;	/// 
			   bil3_1 with a_rs_1 md_rs ;	///
			   a_rs_1 with md_rs ;	///
			   ///
			   z_`v'_1 d_`v' on  welle_1 westost_1 sex_1 a_rs_1 bil3_1 md_rs ;	///
			   	///
			   model ind:	///
			   z_`v'_2 on z_`v'_1@1; 	///
			   d_`v' by z_`v'_2@1;	///
			   d_`v' on z_`v'_1;	///
			   z_`v'_2@0;  	///
			   	///
			   [z_`v'_1](id2); [d_`v'](cd); 	///
			   [z_`v'_2@0 welle_1@0 westost_1@0 sex_1@0 bil3_1@0 a_rs_1@0 md_rs@0 ]; 	///
			   	///
			   welle_1 	with westost_1 sex_1 bil3_1 a_rs_1 md_rs ;	///
			   westost_1 with sex_1 bil3_1 a_rs_1 md_rs ;	/// 
			   sex_1 	with bil3_1 a_rs_1 md_rs ;	/// 
			   bil3_1 with a_rs_1 md_rs ;	///
			   a_rs_1 with md_rs ;	///
			   ///
			   z_`v'_1 d_`v' on  welle_1 westost_1 sex_1 a_rs_1 bil3_1 md_rs ;	///
			   	///
			   MODEL CONSTRAINT:	///
			   !id = 0; 	///
			   !id2 = 0;	///
			   !cd = 0;		///
			   ///
			   )	///
			   title("FREE INTERCEPTS AND FIXED SLOPES")	///
			   iterations(10000) type(general) variable(GROUPING = erw_1(0=dir 1=ind))	///
			   SAVELOGfile($path_log\eink_fin) //log(off)
			   
	di "*****************************" _n "***** `v' ~ FREE INTERCEPTS AND FIXED SLOPES *********"			   
	di "status: `r(termination)' - RMSEA: `r(RMSEA)'	- Chi2:  `r(chisquare)' 	- DF: `r(chisquare_df)'"
		

		
	
		
** Anzphy: gleicher Intercept, gleiche Slopes

	cap restore, not
	preserve
	local v anzphy
	di "~~~~~~ variable `v' "
	
	sum		`v'_1	
	gen z_`v'_1 = ( `v'_1 - `r(mean)' ) / `r(sd)'	
	gen z_`v'_2 = ( `v'_2 - `r(mean)' ) / `r(sd)'	
	
	gen d`v'  = (`v'_2 - `v'_1) 
	sum d`v'
	gen zd`v' = ( d`v' - `r(mean)' ) / `r(sd)'
	sum zd`v' 
	
	runmplus   z_`v'_1 z_`v'_2 erw_1 welle_1 westost_1 sex_1 a_rs_1 bil3_1 md_rs , 			///
		model( z_`v'_2 on z_`v'_1@1; 		///
			   d_`v' by z_`v'_2@1;	///
			   d_`v' on z_`v'_1;	///
			   z_`v'_2@0;  	///
							///
			   [z_`v'_1](id); [d_`v'](cd); 	///
			   [z_`v'_2@0 welle_1@0 westost_1@0 sex_1@0 bil3_1@0 a_rs_1@0 md_rs@0 ]; 	///
			   ///
			   welle_1 	with westost_1 sex_1 bil3_1 a_rs_1 md_rs ;	///
			   westost_1 with sex_1 bil3_1 a_rs_1 md_rs ;	/// 
			   sex_1 	with bil3_1 a_rs_1 md_rs ;	/// 
			   bil3_1 with a_rs_1 md_rs ;	///
			   a_rs_1 with md_rs ;	///
			   ///
			   z_`v'_1 d_`v' on  welle_1 westost_1 sex_1 a_rs_1 bil3_1 md_rs ;	///
			   ///
			   model dir:	///
			   z_`v'_2 on z_`v'_1@1; 	///
			   d_`v' by z_`v'_2@1;	///
			   d_`v' on z_`v'_1;	///
			   z_`v'_2@0;  	///
			   	///
			   [z_`v'_1](id); [d_`v'](cd); 	///
			   [z_`v'_2@0 welle_1@0 westost_1@0 sex_1@0 bil3_1@0 a_rs_1@0 md_rs@0 ]; 	///
			   	///
			   welle_1 	with westost_1 sex_1 bil3_1 a_rs_1 md_rs ;	///
			   westost_1 with sex_1 bil3_1 a_rs_1 md_rs ;	/// 
			   sex_1 	with bil3_1 a_rs_1 md_rs ;	/// 
			   bil3_1 with a_rs_1 md_rs ;	///
			   a_rs_1 with md_rs ;	///
			   ///
			   z_`v'_1 d_`v' on  welle_1 westost_1 sex_1 a_rs_1 bil3_1 md_rs ;	///
			   	///
			   model ind:	///
			   z_`v'_2 on z_`v'_1@1; 	///
			   d_`v' by z_`v'_2@1;	///
			   d_`v' on z_`v'_1;	///
			   z_`v'_2@0;  	///
			   	///
			   [z_`v'_1](id); [d_`v'](cd); 	///
			   [z_`v'_2@0 welle_1@0 westost_1@0 sex_1@0 bil3_1@0 a_rs_1@0 md_rs@0 ]; 	///
			   	///
			   welle_1 	with westost_1 sex_1 bil3_1 a_rs_1 md_rs ;	///
			   westost_1 with sex_1 bil3_1 a_rs_1 md_rs ;	/// 
			   sex_1 	with bil3_1 a_rs_1 md_rs ;	/// 
			   bil3_1 with a_rs_1 md_rs ;	///
			   a_rs_1 with md_rs ;	///
			   ///
			   z_`v'_1 d_`v' on  welle_1 westost_1 sex_1 a_rs_1 bil3_1 md_rs ;	///
			   	///
			   MODEL CONSTRAINT:	///
			   id = 0;	///
			   cd = 0;	///
			   ///
			   )	///
			   title("FREE INTERCEPTS AND FIXED SLOPES")	///
			   iterations(10000) type(general) variable(GROUPING = erw_1(0=dir 1=ind))	///
			   SAVELOGfile($path_log\eink_fin) //log(off)
	di "status: `r(termination)' - RMSEA: `r(RMSEA)'	- Chi2:  `r(chisquare)' 	- DF: `r(chisquare_df)'"	
	
	
** NWAFAM: free Intercept, fixed Slopes

	cap restore, not
	preserve
	local v nwafam
	di "~~~~~~ variable `v' "
	
	sum		`v'_1	
	gen z_`v'_1 = ( `v'_1 - `r(mean)' ) / `r(sd)'	
	gen z_`v'_2 = ( `v'_2 - `r(mean)' ) / `r(sd)'	
	
	gen d`v'  = (`v'_2 - `v'_1) 
	sum d`v'
	gen zd`v' = ( d`v' - `r(mean)' ) / `r(sd)'
	sum zd`v' 
	
	runmplus   z_`v'_1 z_`v'_2 erw_1 welle_1 westost_1 sex_1 a_rs_1 bil3_1 md_rs , 			///
		model( z_`v'_2 on z_`v'_1@1; 		///
			   d_`v' by z_`v'_2@1;	///
			   d_`v' on z_`v'_1;	///
			   z_`v'_2@0;  	///
							///
			   [z_`v'_1](id); [d_`v'](cd); 	///
			   [z_`v'_2@0 welle_1@0 westost_1@0 sex_1@0 bil3_1@0 a_rs_1@0 md_rs@0 ]; 	///
			   ///
			   welle_1 	with westost_1 sex_1 bil3_1 a_rs_1 md_rs ;	///
			   westost_1 with sex_1 bil3_1 a_rs_1 md_rs ;	/// 
			   sex_1 	with bil3_1 a_rs_1 md_rs ;	/// 
			   bil3_1 with a_rs_1 md_rs ;	///
			   a_rs_1 with md_rs ;	///
			   ///
			   z_`v'_1 d_`v' on  welle_1 westost_1 sex_1 a_rs_1 bil3_1 md_rs ;	///
			   ///
			   model dir:	///
			   z_`v'_2 on z_`v'_1@1; 	///
			   d_`v' by z_`v'_2@1;	///
			   d_`v' on z_`v'_1;	///
			   z_`v'_2@0;  	///
			   	///
			   [z_`v'_1](id); [d_`v'](cd); 	///
			   [z_`v'_2@0 welle_1@0 westost_1@0 sex_1@0 bil3_1@0 a_rs_1@0 md_rs@0 ]; 	///
			   	///
			   welle_1 	with westost_1 sex_1 bil3_1 a_rs_1 md_rs ;	///
			   westost_1 with sex_1 bil3_1 a_rs_1 md_rs ;	/// 
			   sex_1 	with bil3_1 a_rs_1 md_rs ;	/// 
			   bil3_1 with a_rs_1 md_rs ;	///
			   a_rs_1 with md_rs ;	///
			   ///
			   z_`v'_1 d_`v' on  welle_1 westost_1 sex_1 a_rs_1 bil3_1 md_rs ;	///
			   	///
			   model ind:	///
			   z_`v'_2 on z_`v'_1@1; 	///
			   d_`v' by z_`v'_2@1;	///
			   d_`v' on z_`v'_1;	///
			   z_`v'_2@0;  	///
			   	///
			   [z_`v'_1](id2); [d_`v'](cd); 	///
			   [z_`v'_2@0 welle_1@0 westost_1@0 sex_1@0 bil3_1@0 a_rs_1@0 md_rs@0 ]; 	///
			   	///
			   welle_1 	with westost_1 sex_1 bil3_1 a_rs_1 md_rs ;	///
			   westost_1 with sex_1 bil3_1 a_rs_1 md_rs ;	/// 
			   sex_1 	with bil3_1 a_rs_1 md_rs ;	/// 
			   bil3_1 with a_rs_1 md_rs ;	///
			   a_rs_1 with md_rs ;	///
			   ///
			   z_`v'_1 d_`v' on  welle_1 westost_1 sex_1 a_rs_1 bil3_1 md_rs ;	///
			   	///
			   MODEL CONSTRAINT:	///
			   id = 0;	///
			   !id2 = 0;	///
			   cd = 0;	///
			   ///
			   )	///
			   title("FREE INTERCEPTS AND FIXED SLOPES")	///
			   iterations(10000) type(general) variable(GROUPING = erw_1(0=dir 1=ind))	///
			   SAVELOGfile($path_log\eink_fin) //log(off)
	di "status: `r(termination)' - RMSEA: `r(RMSEA)'	- Chi2:  `r(chisquare)' 	- DF: `r(chisquare_df)'"	
	

		
** NWFAM: free Intercept, fixed Slopes

	cap restore, not	
	preserve
	local v nwfam
	di "~~~~~~ variable `v' "
	
	sum		`v'_1	
	gen z_`v'_1 = ( `v'_1 - `r(mean)' ) / `r(sd)'	
	gen z_`v'_2 = ( `v'_2 - `r(mean)' ) / `r(sd)'	
	
	gen d`v'  = (`v'_2 - `v'_1) 
	sum d`v'
	gen zd`v' = ( d`v' - `r(mean)' ) / `r(sd)'
	sum zd`v' 
	
	runmplus   z_`v'_1 z_`v'_2 erw_1 welle_1 westost_1 sex_1 a_rs_1 bil3_1 md_rs , 			///
		model( z_`v'_2 on z_`v'_1@1; 		///
			   d_`v' by z_`v'_2@1;	///
			   d_`v' on z_`v'_1;	///
			   z_`v'_2@0;  	///
							///
			   [z_`v'_1](id); [d_`v'](cd); 	///
			   [z_`v'_2@0 welle_1@0 westost_1@0 sex_1@0 bil3_1@0 a_rs_1@0 md_rs@0 ]; 	///
			   ///
			   welle_1 	with westost_1 sex_1 bil3_1 a_rs_1 md_rs ;	///
			   westost_1 with sex_1 bil3_1 a_rs_1 md_rs ;	/// 
			   sex_1 	with bil3_1 a_rs_1 md_rs ;	/// 
			   bil3_1 with a_rs_1 md_rs ;	///
			   a_rs_1 with md_rs ;	///
			   ///
			   z_`v'_1 d_`v' on  welle_1 westost_1 sex_1 a_rs_1 bil3_1 md_rs ;	///
			   ///
			   model dir:	///
			   z_`v'_2 on z_`v'_1@1; 	///
			   d_`v' by z_`v'_2@1;	///
			   d_`v' on z_`v'_1;	///
			   z_`v'_2@0;  	///
			   	///
			   [z_`v'_1](id); [d_`v'](cd); 	///
			   [z_`v'_2@0 welle_1@0 westost_1@0 sex_1@0 bil3_1@0 a_rs_1@0 md_rs@0 ]; 	///
			   	///
			   welle_1 	with westost_1 sex_1 bil3_1 a_rs_1 md_rs ;	///
			   westost_1 with sex_1 bil3_1 a_rs_1 md_rs ;	/// 
			   sex_1 	with bil3_1 a_rs_1 md_rs ;	/// 
			   bil3_1 with a_rs_1 md_rs ;	///
			   a_rs_1 with md_rs ;	///
			   ///
			   z_`v'_1 d_`v' on  welle_1 westost_1 sex_1 a_rs_1 bil3_1 md_rs ;	///
			   	///
			   model ind:	///
			   z_`v'_2 on z_`v'_1@1; 	///
			   d_`v' by z_`v'_2@1;	///
			   d_`v' on z_`v'_1;	///
			   z_`v'_2@0;  	///
			   	///
			   [z_`v'_1](id2); [d_`v'](cd); 	///
			   [z_`v'_2@0 welle_1@0 westost_1@0 sex_1@0 bil3_1@0 a_rs_1@0 md_rs@0 ]; 	///
			   	///
			   welle_1 	with westost_1 sex_1 bil3_1 a_rs_1 md_rs ;	///
			   westost_1 with sex_1 bil3_1 a_rs_1 md_rs ;	/// 
			   sex_1 	with bil3_1 a_rs_1 md_rs ;	/// 
			   bil3_1 with a_rs_1 md_rs ;	///
			   a_rs_1 with md_rs ;	///
			   ///
			   z_`v'_1 d_`v' on  welle_1 westost_1 sex_1 a_rs_1 bil3_1 md_rs ;	///
			   	///
			   MODEL CONSTRAINT:	///
			   id = 0;	///
			   id2 = 0;	///
			   cd = 0;	///
			   ///
			   )	///
			   title("FREE INTERCEPTS AND FIXED SLOPES")	///
			   iterations(10000) type(general) variable(GROUPING = erw_1(0=dir 1=ind))	///
			   SAVELOGfile($path_log\eink_fin) //log(off)
	di "status: `r(termination)' - RMSEA: `r(RMSEA)'	- Chi2:  `r(chisquare)' 	- DF: `r(chisquare_df)'"		
	
	
** AKTANZG: gleicher Intercept, freie Slopes

	cap restore, not
	preserve
	local v aktanzg13
	di "~~~~~~ variable `v' "
	
	sum		`v'_1	
	gen z_`v'_1 = ( `v'_1 - `r(mean)' ) / `r(sd)'	
	gen z_`v'_2 = ( `v'_2 - `r(mean)' ) / `r(sd)'	
	
	gen d`v'  = (`v'_2 - `v'_1) 
	sum d`v'
	gen zd`v' = ( d`v' - `r(mean)' ) / `r(sd)'
	sum zd`v' 
	
	runmplus   z_`v'_1 z_`v'_2 erw_1 welle_1 westost_1 sex_1 a_rs_1 bil3_1 md_rs , 			///
		model( z_`v'_2 on z_`v'_1@1; 		///
			   d_`v' by z_`v'_2@1;	///
			   d_`v' on z_`v'_1;	///
			   z_`v'_2@0;  	///
							///
			   [z_`v'_1](id); [d_`v'](cd); 	///
			   [z_`v'_2@0 welle_1@0 westost_1@0 sex_1@0 bil3_1@0 a_rs_1@0 md_rs@0 ]; 	///
			   ///
			   welle_1 	with westost_1 sex_1 bil3_1 a_rs_1 md_rs ;	///
			   westost_1 with sex_1 bil3_1 a_rs_1 md_rs ;	/// 
			   sex_1 	with bil3_1 a_rs_1 md_rs ;	/// 
			   bil3_1 with a_rs_1 md_rs ;	///
			   a_rs_1 with md_rs ;	///
			   ///
			   z_`v'_1 d_`v' on  welle_1 westost_1 sex_1 a_rs_1 bil3_1 md_rs ;	///
			   ///
			   model dir:	///
			   z_`v'_2 on z_`v'_1@1; 	///
			   d_`v' by z_`v'_2@1;	///
			   d_`v' on z_`v'_1;	///
			   z_`v'_2@0;  	///
			   	///
			   [z_`v'_1](id); [d_`v'](cd); 	///
			   [z_`v'_2@0 welle_1@0 westost_1@0 sex_1@0 bil3_1@0 a_rs_1@0 md_rs@0 ]; 	///
			   	///
			   welle_1 	with westost_1 sex_1 bil3_1 a_rs_1 md_rs ;	///
			   westost_1 with sex_1 bil3_1 a_rs_1 md_rs ;	/// 
			   sex_1 	with bil3_1 a_rs_1 md_rs ;	/// 
			   bil3_1 with a_rs_1 md_rs ;	///
			   a_rs_1 with md_rs ;	///
			   ///
			   z_`v'_1 d_`v' on  welle_1 westost_1 sex_1 a_rs_1 bil3_1 md_rs ;	///
			   	///
			   model ind:	///
			   z_`v'_2 on z_`v'_1@1; 	///
			   d_`v' by z_`v'_2@1;	///
			   d_`v' on z_`v'_1;	///
			   z_`v'_2@0;  	///
			   	///
			   [z_`v'_1](id); [d_`v'](cd2); 	///
			   [z_`v'_2@0 welle_1@0 westost_1@0 sex_1@0 bil3_1@0 a_rs_1@0 md_rs@0 ]; 	///
			   	///
			   welle_1 	with westost_1 sex_1 bil3_1 a_rs_1 md_rs ;	///
			   westost_1 with sex_1 bil3_1 a_rs_1 md_rs ;	/// 
			   sex_1 	with bil3_1 a_rs_1 md_rs ;	/// 
			   bil3_1 with a_rs_1 md_rs ;	///
			   a_rs_1 with md_rs ;	///
			   ///
			   z_`v'_1 d_`v' on  welle_1 westost_1 sex_1 a_rs_1 bil3_1 md_rs ;	///
			   	///
			   MODEL CONSTRAINT:	///
			   id = 0;	///
			   !cd = 0;	///
			   cd2 = 0;	///
			   ///
			   )	///
			   title("FREE INTERCEPTS AND FIXED SLOPES")	///
			   iterations(10000) type(general) variable(GROUPING = erw_1(0=dir 1=ind))	///
			   SAVELOGfile($path_log\eink_fin) //log(off)
	di "status: `r(termination)' - RMSEA: `r(RMSEA)'	- Chi2:  `r(chisquare)' 	- DF: `r(chisquare_df)'"	
	
	
** SUPPORT: gleicher Intercept, fixed Slopes

	cap restore, not	
	preserve
	local v support
	di "~~~~~~ variable `v' "
	
	sum		`v'_1	
	gen z_`v'_1 = ( `v'_1 - `r(mean)' ) / `r(sd)'	
	gen z_`v'_2 = ( `v'_2 - `r(mean)' ) / `r(sd)'	
	
	gen d`v'  = (`v'_2 - `v'_1) 
	sum d`v'
	gen zd`v' = ( d`v' - `r(mean)' ) / `r(sd)'
	sum zd`v' 
	
	runmplus   z_`v'_1 z_`v'_2 erw_1 welle_1 westost_1 sex_1 a_rs_1 bil3_1 md_rs , 			///
		model( z_`v'_2 on z_`v'_1@1; 		///
			   d_`v' by z_`v'_2@1;	///
			   d_`v' on z_`v'_1;	///
			   z_`v'_2@0;  	///
							///
			   [z_`v'_1](id); [d_`v'](cd); 	///
			   [z_`v'_2@0 welle_1@0 westost_1@0 sex_1@0 bil3_1@0 a_rs_1@0 md_rs@0 ]; 	///
			   ///
			   welle_1 	with westost_1 sex_1 bil3_1 a_rs_1 md_rs ;	///
			   westost_1 with sex_1 bil3_1 a_rs_1 md_rs ;	/// 
			   sex_1 	with bil3_1 a_rs_1 md_rs ;	/// 
			   bil3_1 with a_rs_1 md_rs ;	///
			   a_rs_1 with md_rs ;	///
			   ///
			   z_`v'_1 d_`v' on  welle_1 westost_1 sex_1 a_rs_1 bil3_1 md_rs ;	///
			   ///
			   model dir:	///
			   z_`v'_2 on z_`v'_1@1; 	///
			   d_`v' by z_`v'_2@1;	///
			   d_`v' on z_`v'_1;	///
			   z_`v'_2@0;  	///
			   	///
			   [z_`v'_1](id); [d_`v'](cd); 	///
			   [z_`v'_2@0 welle_1@0 westost_1@0 sex_1@0 bil3_1@0 a_rs_1@0 md_rs@0 ]; 	///
			   	///
			   welle_1 	with westost_1 sex_1 bil3_1 a_rs_1 md_rs ;	///
			   westost_1 with sex_1 bil3_1 a_rs_1 md_rs ;	/// 
			   sex_1 	with bil3_1 a_rs_1 md_rs ;	/// 
			   bil3_1 with a_rs_1 md_rs ;	///
			   a_rs_1 with md_rs ;	///
			   ///
			   z_`v'_1 d_`v' on  welle_1 westost_1 sex_1 a_rs_1 bil3_1 md_rs ;	///
			   	///
			   model ind:	///
			   z_`v'_2 on z_`v'_1@1; 	///
			   d_`v' by z_`v'_2@1;	///
			   d_`v' on z_`v'_1;	///
			   z_`v'_2@0;  	///
			   	///
			   [z_`v'_1](id); [d_`v'](cd); 	///
			   [z_`v'_2@0 welle_1@0 westost_1@0 sex_1@0 bil3_1@0 a_rs_1@0 md_rs@0 ]; 	///
			   	///
			   welle_1 	with westost_1 sex_1 bil3_1 a_rs_1 md_rs ;	///
			   westost_1 with sex_1 bil3_1 a_rs_1 md_rs ;	/// 
			   sex_1 	with bil3_1 a_rs_1 md_rs ;	/// 
			   bil3_1 with a_rs_1 md_rs ;	///
			   a_rs_1 with md_rs ;	///
			   ///
			   z_`v'_1 d_`v' on  welle_1 westost_1 sex_1 a_rs_1 bil3_1 md_rs ;	///
			   	///
			   MODEL CONSTRAINT:	///
			   id = 0;	///
			   !cd = 0;	///
			   ///
			   )	///
			   title("FREE INTERCEPTS AND FIXED SLOPES")	///
			   iterations(10000) type(general) variable(GROUPING = erw_1(0=dir 1=ind))	///
			   SAVELOGfile($path_log\eink_fin) //log(off)
	di "status: `r(termination)' - RMSEA: `r(RMSEA)'	- Chi2:  `r(chisquare)' 	- DF: `r(chisquare_df)'"	
	
	
	
* log close
clear
exit
