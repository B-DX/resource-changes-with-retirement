**********************************************************************************************************************
** DATUM LETZTE BEARBEITUNG DO-FILE: 23.01.2012 - `mydate'                                                         	        **
** ERSTER  BEARBEITER:               MWe                                                    			            ** 
** LETZTER BEARBEITER:               MWe                                       				                        **
**********************************************************************************************************************

**********************************************************************************************************************
** DO-FILE: 								                        	    	   				                    **
** BESCHREIBUNG (maximal ausf�hrlich: 		   									**		
**********************************************************************************************************************
/* 

Generierung einer Panelvariable "ruhestzp" - Ruhestandszeitpunkt (Name vom fdz ver�nderbar)

welche den 
 + Zeitpunkt des �bergangs in den Ruhestand abbildet
 + �ber die Wellen harmonisiert wurde nach folgenden Regeln
	* 
	
Ausgangsdatensatz ist der Paneldatensatz (aktuelle Version1: W1-W3)

*/
**********************************************************************************************************************
** 0: VORARBEITEN	 				                                                              				**
**********************************************************************************************************************
local keepvars fallnum welle alter Xpc100 pc101 pc104_2 erw stich pc32 pc103a_1 ///
				Xpc32a pc2_3 pc104_2 pc106 sf36 pc501
use `keepvars' using "$path_in\DEAS_P96-14_l_v0.2.dta", clear
set more off

rename X* *

gen rsj = pc104_2
keep fallnum welle rsj alter pc100 pc101 erw stich pc32 pc103a_1 pc32a pc2_3 pc104_2 pc106 sf36 pc501

tabstat _all, by(welle) s(mean n)
bys welle: tab pc32, nol


* Filter: gr��er/gleich 60 Jahre [ 1 = �ber 60 ]
gen ue60 = 0
replace ue60 = 1 if alter >= 60

* Filter: Panelbefragte
bys fallnum: gen panel = 1 if _n != 1
replace panel = 0 if panel != 1
bys welle: tab panel stich

* Informationsquelle
gen quel_rsj = 0


lab def qu 		0 "Mi: Quelle unklar" 	///
				1 "F1: Person unter 60 Jahre" ///
                2 "F2: Nie erwerbst�tig pc32" ///
				3 "F3: Aktuell kein Rentenbezug" ///
                5 "Wert1: Jahresangabe aus Befr. (!F1, !F2, !F3)" ///
                6 "W2: Fortschreibung in W2 (Panelpers mit !F1, !F2, !F3 und Filter: pc103a_1)" ///
                7 "W2: Jahresang. aus Befr. trotz Panelpers. (mit !F1, !F2, !F3)" ///
				8 "W3: Fortschreibung in W3 (Panelpers mit !F?; Quelle W1 und W2)"	///
				9 "W4: Fortschreibung in W4 (Panelpers mit !F?; Quelle W1, W2 oder W3)"	///
                10 "Problem: W2 Erstbefragte ohne Jahresangabe (mit !F1, !F2, !F3)" ///
				11 "P: W3 Erstbefr. ohne Jahresangabe (mit !F1, !F2, !F3)" ///
				12 "P: W3 Panelbefr. mit Jahresangabe trotz F1 | F2 | F3"  ///
				13 "P: W3 Panelbefr. ohne Jahresangabe (mit !F1, !F2, !F3)" ///
				14 "P: W4 Panelbefr. mit Jahresangabe trotz F1 | F2 | F3"  ///
				15 "P: W4 Panelbefr. ohne Jahresangabe (mit !F1, !F2, !F3)"
				
lab val quel_rsj qu

* 3 FILTERBEDINGUNGEN:
*       a) Person unter 60 Jahre
count if alter < 60 & !mi(rsj)  //ok
replace quel_rsj = 1 if alter < 60 & mi(rsj)

*		b) Person nie hauptberuflich erwerbst�tig
tab pc32 welle if pc32 == 9995, m                               
count if pc32 == 9995 & !mi(rsj)        //ok
tab pc32 pc32a if pc32 > 2014, m
replace quel_rsj = 2 if pc32 == 9995 & mi(rsj)
replace quel_rsj = 2 if pc32a == 3 & mi(rsj)
replace quel_rsj = 2 if pc32 == .c & welle == 1 // in W1: urspr�ngl. Wert 0 (nie erw.t�t. auf -3 gesetzt)

*		c) Aktuell kein Rentenbezug 
tab pc100 welle, m
count if inlist(pc100,2,.a) & !mi(rsj)  //ok
replace quel_rsj = 3 if inlist(pc100,2,.a)

gen filter = 1 if alter <= 60 | pc32 == 9995 | inlist(pc100,2,.a) 
tab rsj filter , m

replace quel_rsj = 5 if filter == . & !mi(rsj)

reshape wide rsj pc100 pc101 ue60 erw panel alter quel_rsj pc2_3 pc32 pc103a_1 filter ///
			pc32a pc104 pc106 sf36 pc501, i(fallnum) j(welle)

* Welle 2: 2 Fragebogenversionen
*'''''''''''''''''''''''''''''''
*	1) Erstbefragte: Frage nach Ruhestanstseintritt kommt f�r alle �ber 60-J�hrige
*	2) Panelbefragte: Ruhestandseintrittsfrage wird �berfiltert
tab panel2
bys panel2: tab rsj2 ue602, m	// 1045 Personen mit �berfilterung

*       zu 1) Erstbefragte (Filterung nach oben genannten Bedingungen)
tab rsj2 if panel2 == 0, m //ok
tab filter2 quel_rsj2 if panel2 == 0 & mi(rsj2), m 
//20 Personen ohne Jahresangabe, trotz Erstbef. & zutreffendem Filter (richtiges Alter) --> Warum?
replace quel_rsj2 = 10 if filter2 == . & panel2 == 0 & mi(rsj2)
tab pc1012 pc1002 if quel_rsj2 == 10, m

tab quel_rsj2 if panel2 == 0, m

*       zu 2) Panelbefragte (Fortschreibung aus Vorwelle)
tab rsj2 if panel2 == 1, m 
//merkw�rdig: eigentlich sollten alle �berfiltert werden, aber ca. 200 Jahresangaben vorhanden
tab rsj2 rsj1 if !mi(rsj2) & panel2 == 1, m
tab quel_rsj2 if panel2 == 1, m
tab filter2 quel_rsj2 if panel2 == 1, m
tab filter2 if panel2 == 1 & rsj2 == .c, m      // Alle Panelf�lle ohne Jahresangabe & Filter aktiv
tab filter2 panel2, m							// 741 Personen sind Panelbefragte, Vorinfo wird verwendet

* Filter gesamt
tab rsj1 rsj2 if panel2 == 1 & filter2 == 1, m
tab filter2 if panel2 == 1 &  mi(rsj1) , m		// 737 Personen mit Missing Wert
tab filter2 if panel2 == 1 & !mi(rsj1) , m 		// in 4 F�llen fehlt die Vorwelleninfo nicht

gen flag = 1			if panel2 == 1 & filter2 == 1 & mi(rsj2) & !mi(rsj2)
replace rsj2 = rsj1 	if panel2 == 1 & filter2 == 1 & mi(rsj2)
replace quel_rsj2 = 6 	if panel2 == 1 & filter2 == 1 & flag == 1
drop flag
// keine Ver�nderung!!!


* Filter nur in W2002-Panel pc103a_1
tab pc103a_12 panel2, m // 1 = 1996 bereits Rente; 2 = 1996 keine Rente; -1 wei� nicht 

tab quel_rsj2 if pc103a_12 == 1
gen flag = 1 			if panel2 == 1 & pc103a_12 == 1 & mi(rsj2)
replace rsj2 = rsj1 	if panel2 == 1 & pc103a_12 == 1 & mi(rsj2)
replace quel_rsj2 = 6 	if panel2 == 1 & pc103a_12 == 1 & flag == 1
drop flag

tab quel_rsj2 if panel2 == 1, m


tab quel_rsj2, m
bro if quel_rsj2 == 0
tab alter2 erw2 if quel_rsj2 == 0, m
// Hier scheinen noch ein paar unklare Daten zu schlummern, die m.E. nur mit der Monatsangabe gehoben
// werden k�nnen.
tab rsj2 	if quel_rsj2 == 0, m
// aber es handelt sich wahrscheinlich nur um 3 Personen mit Jahresangaben




* Welle 3: Filterung nach Panel- und Basisbefragten
*'''''''''''''''''''''''''''''''''''''''''''''''''''

*	1) Erstbefragte: Frage nach Ruhestanstseintritt kommt f�r alle �ber 60-J�hrige
*	2) Panelbefragte: Ruhestandseintrittsfrage wird �berfiltert
tab panel3
bys panel3: tab rsj3 ue603, m	// 1045 Personen mit �berfilterung

*       zu 1) Erstbefragte (Filterung nach oben genannten Bedingungen) 
tab rsj3 if panel3 == 0, m //ok
tab filter3 quel_rsj3 if panel3 == 0 & mi(rsj3), m 
//32 Personen ohne Jahresangabe, trotz Erstbef. & zutreffendem Filter (richtiges Alter) --> Warum?
tab pc1013 pc1003 if quel_rsj3 == 10, m

tab quel_rsj3 if panel3 == 0, m
replace quel_rsj3 = 11 if filter3 == . & panel3 == 0 & mi(rsj3)
tab pc1013 pc1003 if quel_rsj3 == 11, m

tab quel_rsj3 if panel3 == 0, m


*       zu 2) Panelbefragte (Fortschreibung aus Vorwelle)  1,995 - F�lle 
tab rsj3 panel3 if panel3 == 1, m 
//merkw�rdig: eigentlich sollten alle �berfiltert werden, aber 1106 Jahresangaben vorhanden
// kann aber sein: 

gen flag = 1 	if panel3 == 1 & !mi(rsj3)			// Personen mit Ruhestandsangaben in W3
tab flag filter3, m


replace quel_rsj3 = 12 if filter3 == 1 & flag == 1	// Person h�tte eigentlich �berfiltert sein m�ssen, 
													// hat aber einen sinnvollen Wert erhalten

tab quel_rsj3		if filter3 == . & flag == 1		// Quelle schon definiert
drop flag


gen flag = 1 		if panel3 == 1 & mi(rsj3)		// Missingf�lle in W3
tab flag filter3, m

						// Person sollte �berfiltert werde, weil ausgefiltert
tab quel_rsj3			if filter3 == 1 & flag == 1,m	// Quelle schon definiert
tab pc32a3	rsj3 		if filter3 == 1 & flag == 1	 & quel_rsj3 == 0, m // waren schon mal erwerbst�tig
replace quel_rsj3 = 13 	if filter3 == 1 & flag == 1	 & quel_rsj3 == 0 
						// Personen �berfiltert, obwohl nicht F1, F2 oder F3

count if flag == 1 & mi(rsj3) & !mi(rsj2)			// sehr wenige	20 aus W2 und 8 aus W3 fortgeschrieben		
count if flag == 1 & mi(rsj3) & !mi(rsj1)

tab quel_rsj3 if flag == 1 & mi(rsj3) & !mi(rsj2)	// nur die mit unklarer aktueller Quelle �berschreiben		
tab quel_rsj3 if flag == 1 & mi(rsj3) & !mi(rsj1)

replace quel_rsj3 = 8 if quel_rsj3 == 0 & flag == 1 & mi(rsj3) & !mi(rsj2)
replace quel_rsj3 = 8 if quel_rsj3 == 0 & flag == 1 & mi(rsj3) & !mi(rsj1)
replace rsj3 = rsj2 if quel_rsj3 == 8 										// 10 Ver�nderungen
replace rsj3 = rsj1 if quel_rsj3 == 8 	
drop flag

tab quel_rsj3
tab quel_rsj3 pc32a3, m						
								
								

* Welle 4: Filterung nach Panel- und Basisbefragten
*'''''''''''''''''''''''''''''''''''''''''''''''''''

*	1) Erstbefragte: Frage nach Ruhestanstseintritt kommt f�r alle �ber 60-J�hrige
*	2) Panelbefragte: Ruhestandseintrittsfrage wird �berfiltert
tab panel4
bys panel4: tab rsj4 ue604, m	// 1045 Personen mit �berfilterung

*       zu 1) Erstbefragte (Filterung nach oben genannten Bedingungen) 
* keine Erstbefragte in dieser Welle!


*       zu 2) Panelbefragte (Fortschreibung aus Vorwelle)  2,212 - F�lle 
tab rsj4 panel4 if panel4 == 1, m 
//merkw�rdig: eigentlich sollten alle �berfiltert werden, aber 1106 Jahresangaben vorhanden
// kann aber sein: 

gen flag4 = 1 	if panel4 == 1 & !mi(rsj4)			// Personen mit Ruhestandsangaben in W3
tab flag4 filter4, m


replace quel_rsj4 = 14 if filter4 == 1 & flag4 == 1	// Person h�tte eigentlich �berfiltert sein m�ssen, 
													// hat aber einen sinnvollen Wert erhalten

tab quel_rsj4		if filter4 == . & flag4 == 1		// Quelle schon definiert
drop flag4


gen flag4 = 1 		if panel4 == 1 & mi(rsj4)		// Missingf�lle in W3
tab flag4 filter4, m

						// Person sollte �berfiltert werde, weil ausgefiltert
tab quel_rsj4			if filter4 == 1 & flag4 == 1,m	// Quelle schon definiert
tab pc32a4	rsj4 		if filter4 == 1 & flag4 == 1	 & quel_rsj4 == 0, m // waren schon mal erwerbst�tig
replace quel_rsj4 = 15 	if filter4 == 1 & flag4 == 1	 & quel_rsj4 == 0 
						// Personen �berfiltert, obwohl nicht F1, F2 oder F3

count if flag4 == 1 & mi(rsj4) & !mi(rsj2)			// sehr wenige	20 aus W2 und 8 aus W3 fortgeschrieben		
count if flag4 == 1 & mi(rsj4) & !mi(rsj3)
count if flag4 == 1 & mi(rsj4) & !mi(rsj1)

tab quel_rsj4 if flag4 == 1 & mi(rsj4) & !mi(rsj3)	// nur die mit unklarer aktueller Quelle �berschreiben		
tab quel_rsj4 if flag4 == 1 & mi(rsj4) & !mi(rsj2)
tab quel_rsj4 if flag4 == 1 & mi(rsj4) & !mi(rsj1)

replace quel_rsj4 = 9 if quel_rsj4 == 0 & flag4 == 1 & mi(rsj4) & !mi(rsj3)
replace quel_rsj4 = 9 if quel_rsj4 == 0 & flag4 == 1 & mi(rsj4) & !mi(rsj2)
replace quel_rsj4 = 9 if quel_rsj4 == 0 & flag4 == 1 & mi(rsj4) & !mi(rsj1)
replace rsj4 = rsj3 if quel_rsj4 == 9 	
replace rsj4 = rsj2 if quel_rsj4 == 9 										// 10 Ver�nderungen
replace rsj4 = rsj1 if quel_rsj4 == 9 	
drop flag4

tab quel_rsj4
tab quel_rsj4 pc32a4, m											



* Welle 5: Filterung nach Panel- und Basisbefragten
*'''''''''''''''''''''''''''''''''''''''''''''''''''

*	1) Erstbefragte: Frage nach Ruhestanstseintritt kommt f�r alle �ber 60-J�hrige
*	2) Panelbefragte: Ruhestandseintrittsfrage wird �berfiltert
tab panel5
bys panel5: tab rsj5 ue605, m	// 1045 Personen mit �berfilterung

*       zu 1) Erstbefragte (Filterung nach oben genannten Bedingungen) 
* keine Erstbefragte in dieser Welle!


*       zu 2) Panelbefragte (Fortschreibung aus Vorwelle)  2,212 - F�lle 
tab rsj5 panel5 if panel5 == 1, m 
//merkw�rdig: eigentlich sollten alle �berfiltert werden, aber 1106 Jahresangaben vorhanden
// kann aber sein: 

gen flag5 = 1 	if panel5 == 1 & !mi(rsj5)			// Personen mit Ruhestandsangaben in W3
tab flag5 filter5, m


replace quel_rsj5 = 15 if filter5 == 1 & flag5 == 1	// Person h�tte eigentlich �berfiltert sein m�ssen, 
													// hat aber einen sinnvollen Wert erhalten

tab quel_rsj5		if filter5 == . & flag5 == 1		// Quelle schon definiert
drop flag5


gen flag5 = 1 		if panel5 == 1 & mi(rsj5)		// Missingf�lle in W3
tab flag5 filter5, m

						// Person sollte �berfiltert werde, weil ausgefiltert
tab quel_rsj5			if filter5 == 1 & flag5 == 1,m	// Quelle schon definiert
tab pc32a5	rsj5 		if filter5 == 1 & flag5 == 1	 & quel_rsj5 == 0, m // waren schon mal erwerbst�tig
replace quel_rsj5 = 15 	if filter5 == 1 & flag5 == 1	 & quel_rsj5 == 0 
						// Personen �berfiltert, obwohl nicht F1, F2 oder F3

count if flag5 == 1 & mi(rsj5) & !mi(rsj2)			// sehr wenige	20 aus W2 und 8 aus W3 fortgeschrieben		
count if flag5 == 1 & mi(rsj5) & !mi(rsj3)
count if flag5 == 1 & mi(rsj5) & !mi(rsj1)

tab quel_rsj5 if flag5 == 1 & mi(rsj5) & !mi(rsj3)	// nur die mit unklarer aktueller Quelle �berschreiben		
tab quel_rsj5 if flag5 == 1 & mi(rsj5) & !mi(rsj2)
tab quel_rsj5 if flag5 == 1 & mi(rsj5) & !mi(rsj1)

replace quel_rsj5 = 9 if quel_rsj5 == 0 & flag5 == 1 & mi(rsj5) & !mi(rsj3)
replace quel_rsj5 = 9 if quel_rsj5 == 0 & flag5 == 1 & mi(rsj5) & !mi(rsj2)
replace quel_rsj5 = 9 if quel_rsj5 == 0 & flag5 == 1 & mi(rsj5) & !mi(rsj1)
replace rsj5 = rsj3 if quel_rsj5 == 9 	
replace rsj5 = rsj2 if quel_rsj5 == 9 										// 10 Ver�nderungen
replace rsj5 = rsj1 if quel_rsj5 == 9 	
drop flag5

tab quel_rsj5
tab quel_rsj5 pc32a5, m		
								
								
*
* gro�es Finale
*
tab1 quel_rsj?, m



* '''''''''''''''''
* '' Harmonisierung
* '''''''''''''''''
*		1) mit konstanten Angaben �ber die Wellen
egen rmax = rowmax(rsj?)
egen rmin = rowmin(rsj?)

gen rsj_kons = rmax if rmax == rmin						

egen rmiss = rowmiss(rsj?)
replace rsj_kons = .a if rmiss == 5
tab rsj_kons if mi(rsj_kons), m

*		2) mit abweichenden Angaben		965
/* Logik:
	rsj1	rsj2	rsj3	
1	.c		1997	1994	
2	1978	.		1980	
3	1985	.		1965	
4	.c		1997	1998	

Regel 1)
Es werden die Informationen genommen, die n�her am Befragungszeitpunkt liegen:
	Person 1 wurde 2002 (rsj2) gefragt und sagte, es sei 1997 gewesen. 
	Mit steigender zeitlicher Entfernung steigt auch die Distanz (dis?).
	Es handelt sich nur um positive Distanzen, da nur retrospektive Werte.
Regel 2)
	Person 3 hat eine erhebliche Distanz von 20 Jahren. Hier liegt wahrscheinlich ein
	Tippfehler vor. F�lle, die mehr als 10 Jahre abweichen, werden markiert.
*/


gen dis1 = 1996 - rsj1
gen dis2 = 2002 - rsj2
gen dis3 = 2008 - rsj3
gen dis4 = 2011 - rsj4
gen dis5 = 2014 - rsj5

egen dmin = rowmin(dis?)
for any 1 2 3 4 5: replace rsj_kons = rsjX if dmin == disX & !mi(disX) & mi(rsj_kons)
tab rsj_kons if mi(rsj_kons), m		// wenn keine . dann okay!

* letzte Erwerbst�tigkeit auch nach dieser Regel:
gen lastjob = .z
for any 1 2 3 4 5: replace lastjob = pc106X if dmin == disX & !mi(rsj_kons)
tab lastjob , m		// wenn keine . dann okay!
lab var lastjob "vorhergiger Erwerbsstatus"
lab val lastjob x106_de

* Gesundheit
gen sf36_k = .z
gen pc501_k = .z
for any 1 2 3 4 5: replace sf36_k = sf36X if dmin == disX & !mi(rsj_kons)
for any 1 2 3 4 5: replace pc501_k = pc501X if dmin == disX & !mi(rsj_kons)

* Einzelfallentscheidungen bei gro�er Distanz zwischen den Jahren
gen flag0 = -1
for any 1 2 3 4 5: replace flag0 = 1 if abs(rsj_kons - rsjX) > 10 & !mi(rsjX)
tab flag0

* Alter bei Ruhestand
gen a_rs1 = rsj1 - (1996 - alter1)
gen a_rs2 = rsj2 - (2002 - alter2)
gen a_rs3 = rsj3 - (2008 - alter3)
gen a_rs4 = rsj4 - (2011 - alter4)
gen a_rs5 = rsj5 - (2014 - alter5)

* Ausgabe der 410 Problemf�lle  
preserve
keep if flag0 == 1
keep fallnum rsj* rsj_kons a_rs*
reshape long rsj a_rs, i(fallnum) j(welle)
export excel "$path_temp\Einzelfall.xls", replace firstrow(vari)
restore

sort fallnum
bro fallnum rsj? rsj_kons a_rs? if flag0 == 1

* Korrekturen ( --> Einzelfallentscheidung.xls)
replace rsj_kons = 1975 if fallnum == 1000968
replace rsj_kons = 1987 if fallnum == 1001101
replace rsj_kons = 1988 if fallnum == 1001635
replace rsj_kons = .b 	if fallnum == 1001718	// keine Entscheidung mgl.
replace rsj_kons = .b 	if fallnum == 1002084
replace rsj_kons = 1983 if fallnum == 2000141
replace rsj_kons = 1997 if fallnum == 2001115
replace rsj_kons = 1985 if fallnum == 2001999
replace rsj_kons = .b if fallnum == 2002956
replace rsj_kons = 1993 if fallnum == 3000137
replace rsj_kons = 1980 if fallnum == 3000877
replace rsj_kons = 1996 if fallnum == 3001062
replace rsj_kons = 1996 if fallnum == 3001985
replace rsj_kons = 1999 if fallnum == 3002080
replace rsj_kons = 1991 if fallnum == 3002976
replace rsj_kons = 1995 if fallnum == 3003819
replace rsj_kons = 1982 if fallnum == 3004438
replace rsj_kons = 1990 if fallnum == 1000076
replace rsj_kons = 1989 if fallnum == 1000150
replace rsj_kons = 1985 if fallnum == 2001999
replace rsj_kons = 1982 if fallnum == 2002290
replace rsj_kons = 2001 if fallnum == 2002619
replace rsj_kons = 1999 if fallnum == 2002932
replace rsj_kons = 1993 if fallnum == 3000137
replace rsj_kons = .b if fallnum == 3001374
replace rsj_kons = 1996 if fallnum == 3001985
replace rsj_kons = 1996 if fallnum == 3003488
replace rsj_kons = 1991 if fallnum == 3003508


tab rsj_kons, m
count if !mi(rsj_kons)							// 10278 eindeutige Zuordnungen (Personen!)

keep fallnum rsj_kons quel_rsj* rsj* lastjob sf36_k pc501_k

reshape long quel_rsj rsj, i(fallnum) j(welle)

tab2 quel_rsj welle, col

bys fallnum: gen n = _n 
keep if n == 1
keep fallnum rsj_kons 
count if !mi(rsj_kons)

saveold "$path_temp\rsj_kons1_5.dta", replace

* cf _all using "$path_temp\rsj_kons.dta", verbose

**********************************************************************************************************************
** : ENDE                                                                                                           **
**********************************************************************************************************************


exit
